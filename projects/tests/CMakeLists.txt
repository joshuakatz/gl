cmake_minimum_required(VERSION 3.5 FATAL_ERROR)

project(unit_tests CXX C)

set(CMAKE_CXX_STANDARD 11)

# Force gtest to use our linker & compiler rather than discovering their own
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)


include(${CMAKE_SOURCE_DIR}/cmake/googletest/googletest.cmake)
fetch_googletest(
        ${CMAKE_SOURCE_DIR}/cmake
        ${CMAKE_SOURCE_DIR}/vendor/googletest
)
enable_testing()

# Unit testing
add_executable(unit_tests src/libhello/hello.cpp)
target_link_libraries(unit_tests gtest_main hello)

add_test(NAME unit COMMAND ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}/unit_tests)
