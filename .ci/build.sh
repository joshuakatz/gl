#!/usr/bin/env bash
set -e

function build_folder_from_target()
{
    echo "$(pwd)/build-${APP_BUILD_TARGET}"
}

function is_windows()
{
    if [[ "$APP_BUILD_TARGET" = windows* ]];
    then
        return 0
    else
        return 1
    fi
}

function cmake_custom_build_args()
{
    if is_windows;
    then
        echo -Dgtest_disable_pthreads=ON
    fi
}

function ci_make_build_folder() {
    echo "Creating Build Folder"
    [[ -d "$BUILD_FOLDER" ]] || mkdir "${BUILD_FOLDER}"
    cd "${BUILD_FOLDER}"
}

function ci_build()
{
    # Actually Run the Build
    cmake $(cmake_custom_build_args) ..
    make
}

function ci_test()
{
    if is_windows;
    then
        wine ./dist/bin/unit_tests.exe
    else
        ./dist/bin/unit_tests
    fi
}

function main()
{
    ci_make_build_folder
    ci_build
    ci_test
}

export APP_BUILD_TARGET=$1
export BUILD_FOLDER=$(build_folder_from_target)


if [[ -z "$APP_BUILD_TARGET" ]];
then
    echo "No build target provided"
    exit 1
fi

main